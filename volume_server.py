from flask import Flask, render_template, redirect, url_for
from subprocess import call
app = Flask(__name__)

@app.route("/")
def hello():
    return render_template('index.html')

@app.route("/dec/")
def dec():
    call('amixer -D pulse sset Master 5%-'.split(" "))
    return redirect(url_for('hello'))

@app.route("/inc/")
def inc():
    call('amixer -D pulse sset Master 5%+'.split(" "))
    return redirect(url_for('hello'))

@app.route("/set/<int:vol>/")
def setvol():
    if vol in range(100):
        call(f'amixer -D pulse sset Master {vol}%'.split(" "))
    return redirect(url_for('hello'))

if __name__ == "__main__":
    app.run(host='10.0.2.15')
