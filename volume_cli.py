from subprocess import call
import struct

def print_instruction():
    print("LeftClick to decrease volume by 5%")
    print("RightClick to increase volume by 5%")
    print("Ctrl+C to exit from the program")

with open('/dev/input/mice', "rb") as f:
    print_instruction()

    while True:
        try:
            data = f.read(3)
            mouse, x, y = struct.unpack('3b', data)
            cmd = ""

            if mouse == 9:
                cmd = "amixer -D pulse sset Master 5%-"       
            elif mouse == 10:
                cmd = "amixer -D pulse sset Master 5%+"       

            if len(cmd) != 0:
                call(['clear'])
                call(['su', '-c', cmd, 'user'])
                print()
                print_instruction()
                
        except KeyboardInterrupt as e:
            print("Exited")
            exit()
        
